﻿using Neo.SmartContract.Framework;
using Neo.SmartContract.Framework.Services.Neo;
using Neo.SmartContract.Framework.Services.System;
using System;
using System.Numerics;

namespace Poll
{


    public class Poll : SmartContract
    {


        public static object Main(string operation, params object[] args)
        {

            switch (operation)
            {
                case "vote":
                    return Vote((string)args[0], args);
                case "query":
                    return Query((string)args[0]);
                default:
                    return false;
            }
        }

        private static byte[] Query(string voteOption)
        {

            return Storage.Get(Storage.CurrentContext, voteOption);
        }

        private static bool Vote(string voteOption, params object[] args)
        {
            
            if (voteOption == "option1" || voteOption == "option2")
            {
                return VoteInternal(voteOption, args);
            }
            return false;

        }

        private static bool VoteInternal(string voteOption, params object[] args)
        {
            var addr = (string)args[1];
            var old = Storage.Get(Storage.CurrentContext, voteOption);
            if (old == null)
            {
                Storage.Put(Storage.CurrentContext, voteOption, addr);
            }
            else
            {
                Storage.Put(Storage.CurrentContext, voteOption, concatAddress(old, addr));
            }
            return true;
        }

        private static string concatAddress(byte[] old , string addr)
        {
            var res = old.Concat("##".AsByteArray().Concat(addr.AsByteArray())).AsString();
            return res;
        }
    }
}
